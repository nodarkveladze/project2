import com.car.Car;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CarParser {

    private static void sortByModel(List<Car> carsList) {
        Collections.sort(carsList, Comparator.comparing(Car::getModel));
    }

    static void parser(String filePath) throws IOException, ParseException {
        List<Car> carsList = new ArrayList<>();
        JSONParser parser = new JSONParser();
        JSONArray carArray = (JSONArray) parser.parse(new FileReader(filePath));

        for (Object carObject : carArray) {
            JSONObject carObj = (JSONObject) carObject;
            String model = (String) carObj.get("model");
            String manufacturer = (String) carObj.get("manufacturer");
            String transmissionType = (String) carObj.get("transmissionType");
            String fuelType = (String) carObj.get("fuelType");
            String seating = (String) carObj.get("seating");
            Car car = new Car(model, manufacturer, transmissionType, fuelType, seating);
            carsList.add(car);
        }
        sortByModel(carsList);
        carsList.forEach(System.out::println);
    }


}

