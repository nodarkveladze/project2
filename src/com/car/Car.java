package com.car;

public class Car {
    private String model;
    private String manufacturer;
    private String transmissionType;
    private String fuelType;
    private String seating;

    public Car(String model, String manufacturer, String transmissionType, String fuelType, String seating) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.transmissionType = transmissionType;
        this.fuelType = fuelType;
        this.seating = seating;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getSeating() {
        return seating;
    }

    public void setSeating(String seating) {
        this.seating = seating;
    }

    public String toString() {
        return this.model + " " + this.manufacturer + " " + this.transmissionType + " " + this.fuelType + " " + this.seating;
    }
}
